import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import path from 'path';
import mongoose from 'mongoose';
import router from './routes';

// coneccions mongo
mongoose.Promise = global.Promise;
const dburl = 'mongodb://localhost:27017/dbsistema';
mongoose.connect(dburl, {useNewUrlParser: true, useCreateIndex: true})
.then(mongoose => console.log('Conectado MongoBD: 27017'))
.catch(err => console.log(err));

const app = express();

app.use(morgan('dev'));
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', router);

app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'), () => {
  console.log('Server on port', app.get('port'));
  console.log(path.join(__dirname, 'public'));
});























